﻿const {inspect} = require('util'); //for debugging

'use strict';

class DocFinder {

  /** Constructor for instance of DocFinder. */
  constructor() {
	this.setOfNoiseWords = new Set();
	this.mapOfWords = new Map();
	this.mapOfFiles = new Map();
	this.mapOfDocuments = new Map();
	this.mapOfOffset = new Map();
	this.mapOfOffsets = new Map();
	this.results = new Map();
	this.final = [];
  }

  /** Return array of non-noise normalized words from string content.
   *  Non-noise means it is not a word in the noiseWords which have
   *  been added to this object.  Normalized means that words are
   *  lower-cased, have been stemmed and all non-alphabetic characters
   *  matching regex [^a-z] have been removed.
   */
  words(content) {
	var tempArray = content.split(/\s+/);
	var array = [];
		tempArray.forEach(function(value){
		var tempWord = normalize(value);
		if(! this.setOfNoiseWords.has(tempWord)){
			array.push(tempWord)
		}
	},this);
    return array;
  }

  /** Add all normalized words in noiseWords string to this as
   *  noise words. 
   */
  addNoiseWords(noiseWords) {
	 
	  var arr = noiseWords.split("\n");
	  this.setOfNoiseWords = new Set(arr);
	      			    
  }

  /** Add document named by string name with specified content to this
   *  instance. Update index in this with all non-noise normalized
   *  words in content string.
   */ 
  addContent(name, content) {
		this.mapOfDocuments.set(name,content);
		this.mapOfWords = new Map();
		var temp = content.split(/\s+/);
		temp.forEach(function(value){
		var tempWord = normalize(value);
		if(! this.setOfNoiseWords.has(tempWord)){
			this.mapOfWords.has(tempWord) ? this.mapOfWords.set(tempWord,this.mapOfWords.get(tempWord)+1) : this.mapOfWords.set(tempWord,1);
		}
	},this);
	let match;
	this.mapOfOffset = new Map();
        while (match = WORD_REGEX.exec(content)) {
          const [word, offset] = [match[0], match.index];
	  if(!this.mapOfOffset.has(normalize(match[0])))
          this.mapOfOffset.set(normalize(match[0]),match.index);
        }
	this.mapOfFiles.set(name,this.mapOfWords);
	this.mapOfOffsets.set(name,this.mapOfOffset);
  }

  /** Given a list of normalized, non-noise words search terms, 
   *  return a list of Result's  which specify the matching documents.  
   *  Each Result object contains the following properties:
   *     name:  the name of the document.
   *     score: the total number of occurrences of the search terms in the
   *            document.
   *     lines: A string consisting the lines containing the earliest
   *            occurrence of the search terms within the document.  Note
   *            that if a line contains multiple search terms, then it will
   *            occur only once in lines.
   *  The Result's list must be sorted in non-ascending order by score.
   *  Results which have the same score are sorted by the document name
   *  in lexicographical ascending order.
   *
   */
  find(terms) {
		var temp ="" ;
		var arr = [];
		var map = new Map();
		var mapOfLine = new Map();
		this.results = new Map();
		this.final = [];
		terms.forEach(function(value){
		for (let [key, val] of this.mapOfFiles) {
			if(val.has(value)){
					this.results.has(key)?this.results.set(key,this.results.get(key)+val.get(value)):this.results.set(key,val.get(value));
				}
			}
},this);
		terms.forEach(function(term){
		for(let [key,value] of this.mapOfOffsets){
			if(value.has(term)){
			if(!map.has(key) || map.get(key) > value.get(term)){
			map.set(key,value.get(term));		
		}
	}
	}
},this);

		for(let[key,value] of map){
		var arr1 = "";
		var index = value;
		var content =this.mapOfDocuments.get(key); 
		while(content.charAt(index) != "\n"){
			arr1=arr1+ content.charAt(index);
			index++;
			}
		var arr2= "";
		index = value - 1;
		while(content.charAt(index) != "\n" && index > 0)
			{
			arr2=arr2+ content.charAt(index);
			index--;
			}
		arr2 = arr2.split("").reverse().join("");
		arr1 = arr2+arr1+"\n";
		map.set(key,arr1);	
		}
			
				for(let [key,value] of this.results){
				this.final.push(new Result(key,value,map.get(key)));				  
				}
				
	this.final = insertionSort(this.final);

    return this.final;
  }

  /** Given a text string, return a ordered list of all completions of
   *  the last word in text.  Returns [] if the last char in text is
   *  not alphabetic.
   */
  complete(text) {
  var completeWords = new Set();
  var expr =RegExp(text+'[a-z]*');

		for (let [key, val] of this.mapOfFiles) {
			for(let [k,v] of val){
				if(k.match(expr) != null)
				completeWords.add(k); 
				}
			}
let completeWord = Array.from(completeWords);
    return completeWord.sort();
  }
} //class DocFinder

module.exports = DocFinder;

/** Regex used for extracting words as maximal non-space sequences. */
const WORD_REGEX = /\S+/g;

/** A simple class which packages together the result for a 
 *  document search as documented above in DocFinder.find().
 */ 
class Result {
  constructor(name, score, lines) {
    this.name = name; this.score = score; this.lines = lines;
  }

  toString() { return `${this.name}: ${this.score}\n${this.lines}`; }
}

/** Compare result1 with result2: higher scores compare lower; if
 *  scores are equal, then lexicographically earlier names compare
 *  lower.
 */
function compareResults(result1, result2) {
  return (result2.score - result1.score) ||
    result1.name.localeCompare(result2.name);
}

/** Normalize word by stem'ing it, removing all non-alphabetic
 *  characters and converting to lowercase.
 */
function normalize(word) {
  return stem(word.toLowerCase()).replace(/[^a-z]/g, '');
}

/** Place-holder for stemming a word before normalization; this
 *  implementation merely removes 's suffixes.
 */
function stem(word) {
  return word.replace(/\'s$/, '');
}

function insertionSort(array) {
  for(var i = 0; i < array.length; i++) {
    var temp = array[i];
    var j = i - 1;
    while (j >= 0 && compareResults(array[j],temp) > 0) {
      array[j + 1] = array[j];
      j--;
    }
    array[j + 1] = temp;
  }
  return array;
}
