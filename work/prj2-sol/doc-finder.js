const assert = require('assert');
const mongo = require('mongodb').MongoClient;

const {inspect} = require('util'); //for debugging
const {promisify} = require('util');

'use strict';

const DB_TABLE = 'docInfo';
const DB_TABLE_Noise = 'noiseWords';
/** This class is expected to persist its state.  Hence when the
 *  class is created with a specific database url, it is expected
 *  to retain the state it had when it was last used with that URL.
 */ 
class DocFinder {

  /** Constructor for instance of DocFinder. The dbUrl is
   *  expected to be of the form mongodb://SERVER:PORT/DB
   *  where SERVER/PORT specifies the server and port on
   *  which the mongo database server is running and DB is
   *  name of the database within that database server which
   *  hosts the persistent content provided by this class.
   */
  constructor(dbUrl) {
	this.clientURL = dbUrl.substring(0,dbUrl.lastIndexOf('/'));
	this.dbName = dbUrl.substring(dbUrl.lastIndexOf('/')+1);
	this.client = null;
	this.db = null;
  }

  /** This routine is used for all asynchronous initialization
   *  for instance of DocFinder.  It must be called by a client
   *  immediately after creating a new instance of this.
   */
  async init() {
	this.client = await mongo.connect(this.clientURL,MONGO_OPTIONS);
	this.db = this.client.db(this.dbName);
	this.dbTable = this.db.collection(DB_TABLE);
	this.dbTable_Noise = this.db.collection(DB_TABLE_Noise);
	//this.setOfNoiseWords = [];
  }

  /** Release all resources held by this doc-finder.  Specifically,
   *  close any database connections.
   */
  async close() {
    this.client.close();
  }

  /** Clear database */
  async clear() {
	await this.db.dropDatabase();
  }

  /** Return an array of non-noise normalized words from string
   *  contentText.  Non-noise means it is not a word in the noiseWords
   *  which have been added to this object.  Normalized means that
   *  words are lower-cased, have been stemmed and all non-alphabetic
   *  characters matching regex [^a-z] have been removed.
   */
  async words(contentText) {
    var tempArray = contentText.split(/\s+/); 
    let arrayOfNoiseWords = await this.dbTable_Noise.find().toArray();
	let setOfNoiseWords = new Set();
	for (let val of arrayOfNoiseWords) {
		setOfNoiseWords.add(val._id);
	}
	var array = [];
		tempArray.forEach(function(value){
		var tempWord = normalize(value);
		if(setOfNoiseWords.has(tempWord) != -1){
			array.push(tempWord)
		}
	},this);
	//console.log(array)
    return array;
  }

  /** Add all normalized words in the noiseText string to this as
   *  noise words.  This operation should be idempotent.
   */
  async addNoiseWords(noiseText) {
    var arr = noiseText.split("\n");
    var setOfNoiseWords = new Set(arr);
	//console.log(setOfNoiseWords);
	for(let noiseWord of setOfNoiseWords ){
		const ret  = await this.dbTable_Noise.updateOne({_id:noiseWord},{$set: { _id: noiseWord}},{upsert:true});
	}

	//const ret = await this.dbTable_Noise.find({_id:"a"}).toArray();
	//console.log(ret[0]._id);
  }

  /** Add document named by string name with specified content string	
   *  contentText to this instance. Update index in this with all
   *  non-noise normalized words in contentText string.
   *  This operation should be idempotent.
   */ 
  async addContent(name, contentText) {
	//console.log("printing: "+ name);
	//console.log(contentText);
let arrayOfNoiseWords = await this.dbTable_Noise.find().toArray();
	let setOfNoiseWords = new Set();
	for (let val of arrayOfNoiseWords) {
		setOfNoiseWords.add(val._id);
	}
	let mapOfWords = new Map();
		var temp = contentText.split(/\s+/);
		temp.forEach(function(value){
		var tempWord = normalize(value);
		if(! setOfNoiseWords.has(tempWord)){
			mapOfWords.has(tempWord) ? mapOfWords.set(tempWord,mapOfWords.get(tempWord)+1) : mapOfWords.set(tempWord,1);
		}
	});
	let match;
	let mapOfOffset = new Map();
        while (match = WORD_REGEX.exec(contentText)) {
          const [word, offset] = [match[0], match.index];
	  if(!mapOfOffset.has(normalize(match[0])))
          mapOfOffset.set(normalize(match[0]),match.index);
        }
	
	//this.mapOfFiles.set(name,this.mapOfWords);
	//this.mapOfOffsets.set(name,this.mapOfOffset);
	const ret = await this.dbTable.updateOne({_id:name},{$set: {_id:name, content:contentText, count:mapOfWords, offset:mapOfOffset }}, {upsert: true});
	//console.log(ret.upsertedId);
	//assert(ret.upsertedId == name);
  }

  /** Return contents of document name.  If not found, throw an Error
   *  object with property code set to 'NOT_FOUND' and property
   *  message set to `doc ${name} not found`.
   */
  async docContent(name) {
	
    const ret = await this.dbTable.find({_id:name}).toArray();
	//console.log(ret[0]);
	if(ret[0] == undefined) {
		throw new Error('NOT FOUND');
	}
	
	//let map = ret[0].count;
	//console.log(Object.keys(map));
    return ret[0].content;
  }
  
  /** Given a list of normalized, non-noise words search terms, 
   *  return a list of Result's  which specify the matching documents.  
   *  Each Result object contains the following properties:
   *
   *     name:  the name of the document.
   *     score: the total number of occurrences of the search terms in the
   *            document.
   *     lines: A string consisting the lines containing the earliest
   *            occurrence of the search terms within the document.  The 
   *            lines must have the same relative order as in the source
   *            document.  Note that if a line contains multiple search 
   *            terms, then it will occur only once in lines.
   *
   *  The returned Result list must be sorted in non-ascending order
   *  by score.  Results which have the same score are sorted by the
   *  document name in lexicographical ascending order.
   *
   */
  async find(terms) {

   var temp ="" ;
		var arr = [];
		let setOfCount = new Set();
		var map = new Map();
		var mapOfLine = new Map();
		let results = new Map();
		
		let final = [];
		const ret = await this.dbTable.find().toArray();
		terms.forEach(function(value){
		for (let val of ret) {
			let content = val.content;
			let count = val.count;
			let key = Object.keys(count);
			let set1 = new Set(key);
			if(set1.has(value)){
					results.has(val._id)?results.set(val._id,results.get(val._id)+count[value]):results.set(val._id,count[value]);
				}
			let offset_Object = val.offset;
 			let key2 = Object.keys(offset_Object);
			let set2 = new Set(key2);
			if(set2.has(value)){
				let offset = offset_Object[value];
				let line = content.substring(content.lastIndexOf('\n', offset)+1,(content + '\n').indexOf('\n', offset)).replace('\n', '');
				//console.log(line);
				if(mapOfLine.has(val._id)){
					if( mapOfLine.get(val._id) !== line){
				
						mapOfLine.set(val._id,mapOfLine.get(val._id)+'\n'+line);
					}
				}else{
				mapOfLine.set(val._id,line);
				}
//				mapOfLine.has(val._id)? (!mapOfLine.get(val._id)==line) ? mapOfLine.set(val._id,mapOfLine.get(val._id)+'\n'+line) : "" : mapOfLine.set(val._id,line);
			}
}
}); 

				for(let [key,value] of results){
				final.push(new Result(key,value,mapOfLine.get(key)));				  
				}

//console.log(mapOfLine);

    return final.sort(compareResults);
  }

  /** Given a text string, return a ordered list of all completions of
   *  the last normalized word in text.  Returns [] if the last char
   *  in text is not alphabetic.
   */
  async complete(text) {
 var completeWords = new Set();
  var expr =RegExp(text+'[a-z]*');
    const ret = await this.dbTable.find().toArray();
	for(let value of ret){
		let array = Object.keys(value.count);
		for(let key of array){
			if(key.match(expr) != null){
			completeWords.add(key);
		    }
		} 
	}

let completeWord = Array.from(completeWords);
//console.log(completeWord);
    return completeWord.sort();
  }

  //Add private methods as necessary

} //class DocFinder

module.exports = DocFinder;

//Add module global functions, constants classes as necessary
//(inaccessible to the rest of the program).

//Used to prevent warning messages from mongodb.
const MONGO_OPTIONS = {
  useNewUrlParser: true
};

/** Regex used for extracting words as maximal non-space sequences. */
const WORD_REGEX = /\S+/g;

/** A simple utility class which packages together the result for a
 *  document search as documented above in DocFinder.find().
 */ 
class Result {
  constructor(name, score, lines) {
    this.name = name; this.score = score; this.lines = lines;
  }

  toString() { return `${this.name}: ${this.score}\n${this.lines}`; }
}

/** Compare result1 with result2: higher scores compare lower; if
 *  scores are equal, then lexicographically earlier names compare
 *  lower.
 */
function compareResults(result1, result2) {
  return (result2.score - result1.score) ||
    result1.name.localeCompare(result2.name);
}

/** Normalize word by stem'ing it, removing all non-alphabetic
 *  characters and converting to lowercase.
 */
function normalize(word) {
  return stem(word.toLowerCase()).replace(/[^a-z]/g, '');
}

/** Place-holder for stemming a word before normalization; this
 *  implementation merely removes 's suffixes.
 */
function stem(word) {
  return word.replace(/\'s$/, '');
}



