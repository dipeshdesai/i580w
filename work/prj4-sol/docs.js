'use strict';

const express = require('express');
const upload = require('multer')();
const fs = require('fs');
const mustache = require('mustache');
const Path = require('path');
const { URL } = require('url');
const querystring = require('querystring');

const STATIC_DIR = 'statics';
const TEMPLATES_DIR = 'templates';

function serve(port, base, model) {
  const app = express();
  app.locals.port = port;
  app.locals.base = base;
  app.locals.model = model;
  process.chdir(__dirname);
  app.use(base, express.static(STATIC_DIR));
  setupTemplates(app, TEMPLATES_DIR);
  setupRoutes(app);
  app.listen(port, function() {
    console.log(`listening on port ${port}`);
  });
}


module.exports = serve;

/******************************** Routes *******************************/

function setupRoutes(app) {
 const base = app.locals.base;
//console.log(app);
app.use('*', (req, res, next) => { console.log(req.originalUrl); next() })

app.get(`${base}/add.html`,addContent(app));
app.post(`${base}/add.html`,upload.single('file'),doAddContent(app));
app.get(`${base}/search.html`,doSearchContent(app));   
app.get(`${base}/:name`,doGetContent(app));
app.get(`/`,redirect(app)); 
  
}

/*************************** Action Routines ***************************/
function addContent(app){
 return async function(req,res){ 

const model = {base: app.locals.base};
const html = doMustache(app, 'add', model);
    res.send(html);
};		
};

function doAddContent(app){
 return async function(req,res){ 
//console.log(req.body);
const add = req.body.submit === 'add';
let name,errors ;

try{
if(add){
        if(req.file === undefined){	
	errors = 'please select a file containing a document to upload';
	}
	else{
	name = req.file.originalname;
	name = name.substring(0,name.indexOf('.'));
 	
        let val = req.file.buffer.toString('ascii', 0, req.file.buffer.length);
	
	await app.locals.model.addContent(name,val);

	res.redirect(`${app.locals.base}/${name}`);
	} 	

}
}catch(err){
errors = wsErrors(err);
}
if (errors) {
  const model = {base: app.locals.base ,errors: errors};
  const html = doMustache(app, 'add', model);
  res.send(html);
    }

};		
};

function doSearchContent(app) {
  return async function(req, res) {

    const isSubmit = (req.query.submit !== undefined);
    let results = [];
    let errors = undefined;
    let result = [];
    let r;
    let prev = undefined;
    let next = undefined;
    const search = getNonEmptyValues(req.query);
    let model;
    if (isSubmit || req.originalUrl.includes('&start')) {   	   
	
      if ( search.q === undefined) {
	errors = 'please specify one-or-more search terms';
	model = {base: app.locals.base, errors1: errors};
      }
//}
//console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$: ",relativeUrl(req), req.originalUrl);
      if (!errors) {
	try {
	   const q = querystring.stringify(search);
	   //console.log("q   "+q);
	  results = await app.locals.model.find(q);
	console.log(results.length);
	}
	catch (err) {
          console.error(err);
	  errors = wsErrors(err);
	}
	if (results.results.length == 0) {
	  errors = search.q;
	  model = {base: app.locals.base, errors2: errors};
	}else{
	let len = results.results.length;
	//console.log(typeof search.q);
	let searchterms = search.q.split(' ');
	console.log(searchterms);
	let newLine;
	for(let i =0;i< len;i++){
	r = results.results[i];
	let temp = r.lines[0];
	for(let searchterm of searchterms){
		let regexp = new RegExp(searchterm,'gi');
			if(temp.match(regexp))
			temp = temp.replace(regexp,'<span class="search-term">' +temp.match(regexp)[0]+ '</span>');
	}

	r.lines = temp;//r.lines.join(' ');
	//console.log(r.lines);	
	r.href = relativeUrl(req);
	r.href = r.href.substring(0,r.href.lastIndexOf('/')+1)+r.name;
	result[i] = r;
	}
	let links = results.links;
	if(links != undefined){
	for(let link of links){
	if(link.rel == 'self'){
		link.href = link.href.split("&count")[0];
	}
	if(link.rel == 'next'){
		link.href = link.href.split("&count")[0];
		next = relativeUrl(req)+link.href.substring(link.href.indexOf('?'));
		//next = link.href;
		//console.log(next);
	}
	if(link.rel == 'previous'){
		link.href = link.href.split("&count")[0];
		prev = relativeUrl(req)+link.href.substring(link.href.indexOf('?'));
      	}
	}
}
	}
      }
}
	if(!errors){
	model = {base: app.locals.base,results: results, result: result,next: next,previous: prev}
		//console.log("\n\nModel: ",model.results);
	}
	//console.log(model);
    const html = doMustache(app, 'search', model);
    res.send(html);
  };
};


function redirect(app){
return async function(req,res){
const base = app.locals.base;
res.redirect(`${base}/`);
};
};

function doGetContent(app) {
  return async function(req, res) {
    let model;	
   // console.log(req.params);
    const name = req.params.name;
	//console.log(name);
    try {
      const results = await app.locals.model.docContent(name);
	//console.log(results);
	if(results.content === undefined){
	model = { base: app.locals.base, errors:` doc ${name} not found `};
    }
      else{
      model = { base: app.locals.base, name: name, content: results.content };
     }
    }
    catch (err) {
      //console.error(err);
      const errors = wsErrors(err);
      model = errorModel(app,{}, errors);
    }
   console.log(model);
const html = doMustache(app,'getContent',model);
    res.send(html);
  };
};


/************************ General Utilities ****************************/
function validate(values, requires=[]) {
  const errors = {};
  requires.forEach(function (name) {
    if (values[name] === undefined) {
      errors[name] =
	`A value for '${FIELDS_INFO[name].friendlyName}' must be provided`;
    }
  });
  for (const name of Object.keys(values)) {
    const fieldInfo = FIELDS_INFO[name];
    const value = values[name];
    if (fieldInfo.regex && !value.match(fieldInfo.regex)) {
      errors[name] = fieldInfo.error;
    }
  }
  return Object.keys(errors).length > 0 && errors;
}

/** return object containing all non-empty values from object values */
function getNonEmptyValues(values) {
  const out = {};
  Object.keys(values).forEach(function(k) {
    const v = values[k];
    if (v && v.trim().length > 0) out[k] = v.trim();
  });
  return out;
}


/** Return a URL relative to req.originalUrl.  Returned URL path
 *  determined by path (which is absolute if starting with /). For
 *  example, specifying path as ../search.html will return a URL which
 *  is a sibling of the current document.  Object queryParams are
 *  encoded into the result's query-string and hash is set up as a
 *  fragment identifier for the result.
 */
function relativeUrl(req, path='', queryParams={}, hash='') {
  const url = new URL('http://dummy.com');
  url.protocol = req.protocol;
  url.hostname = req.hostname;
  url.port = req.socket.address().port; 
  url.pathname = req.originalUrl.replace(/(\?.*)?$/, '');
  if (path.startsWith('/')) {
    url.pathname = path;
  }
  else if (path) {
    url.pathname += `/${path}`;
  }
  url.search = '';
  Object.entries(queryParams).forEach(([k, v]) => {
    url.searchParams.set(k, v);
  });
  url.hash = hash;
  return url.toString();
}

function wsErrors(err) {
  const msg = (err.message) ? err.message : 'web service error';
  //console.error(msg);
  return { _: [ msg ] };
}

function errorModel(app, values={}, errors={}) {
  return {
    base: app.locals.base,
    errors: errors._,
   // fields: fieldsWithValues(values, errors)
  };
}
/************************** Template Utilities *************************/


/** Return result of mixing view-model view into template templateId
 *  in app templates.
 */
function doMustache(app, templateId, view) {
//console.log(app.templates);
  const templates = { footer: app.templates.footer };

  return mustache.render(app.templates[templateId], view, templates);
}

/** Add contents all dir/*.ms files to app templates with each 
 *  template being keyed by the basename (sans extensions) of
 *  its file basename.
 */
function setupTemplates(app, dir) {
  app.templates = {};
  for (let fname of fs.readdirSync(dir)) {
    const m = fname.match(/^([\w\-]+)\.ms$/);
    if (!m) continue;
    try {
      app.templates[m[1]] =
	String(fs.readFileSync(`${TEMPLATES_DIR}/${fname}`));
    }
    catch (e) {
      console.error(`cannot read ${fname}: ${e}`);
      process.exit(1);
    }
  }
}

