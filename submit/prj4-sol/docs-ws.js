'use strict';

const axios = require('axios');


function DocsWs(baseUrl) {
  this.docsUrl = `${baseUrl}/docs`;
}

module.exports = DocsWs;

DocsWs.prototype.find = async function(q) {
  try {
    const url = this.docsUrl + ((q === undefined) ? '' : `?${q}`);
    const response = await axios.get(url);
    return response.data;
  }
  catch (err) {
    console.error(err);
    throw (err.response && err.response.data) ? err.response.data : err;
  }
};

DocsWs.prototype.docContent = async function(name) {
  try {
    const url = this.docsUrl +'/' +name;
    const response = await axios.get(url);
    return response.data;
  }
  catch (err) {
    console.error(err);
    throw (err.response && err.response.data) ? err.response.data : err;
  }  
};


DocsWs.prototype.addContent = async function(name,content) {
  try {
    const response = await axios.post(this.docsUrl,{name:name,content: content});
    return response.data;
  }
  catch (err) {
    console.error(err);
    throw (err.response && err.response.data) ? err.response.data : err;
  }
};
