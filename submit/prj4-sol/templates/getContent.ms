<!DOCTYPE html>
<html>
  <head>
    <title>test1</title>
    <link rel="stylesheet" href="/docs/css/style.css">
  </head>
  <body>
    <h1 class="doc-name">{{name}}</h1>
    <ul>
	{{#errors}}<li class="error">{{errors}}</li>{{/errors}}
    </ul>
    <pre class="content">
{{content}}
</pre>

<footer>
<ul>
<li><a id="home" href="/docs/index.html">Home</a></li>
<li><a id="add" href="/docs/add.html">Document Add</a></li>
<li><a id="search" href="/docs/search.html">Document Search</a></li>
</ul>
</footer>
  </body>
</html>
