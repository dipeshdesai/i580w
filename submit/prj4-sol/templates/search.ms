<!DOCTYPE html>
<html>
  <head>
    <title>Documents Collections Search</title>
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <h1>Documents Collection Search</h1>
    <ul>
	{{#errors2}}
        <li class="error">no document containing "{{errors2}}" found; please retry}</li>
	{{/errors2}}
    </ul>
    <form method="GET" action="/docs/search.html">
      <p>
        Please fill in one or more search terms in the field below:
      </p>
      <label>
        <span class="label">Search Terms:</span>
          <input id="query" name="q" value="">
	</label>
	<br/>
	<span class="error">{{errors1}}</span><br/>
      </label>
      <input id="submit" name="submit" type="submit"
             value="search" class="control">
    </form>
{{#results}}
	<h2>Search Results</h2>
{{#result}}
        <p class="result">
	  <a class="doc-name" href={{href}}>{{name}}</a><br>
	  {{{lines}}}
<br>
	</p>

{{/result}}

{{#previous}}
<a id="previous" href="{{previous}}">Prev</a>
{{/previous}}

{{#next}}
<a id="next" href="{{next}}">Next</a>
{{/next}}
{{/results}}
<footer>
<ul>
<li><a id="home" href="/docs/index.html">Home</a></li>
<li><a id="add" href="/docs/add.html">Document Add</a></li>
<li><a id="search" href="/docs/search.html">Document Search</a></li>
</ul>
</footer>
  </body>
</html>
