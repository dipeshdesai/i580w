'use strict';

const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const process = require('process');
const url = require('url');
const queryString = require('querystring');

const OK = 200;
const CREATED = 201;
const BAD_REQUEST = 400;
const NOT_FOUND = 404;
const CONFLICT = 409;
const SERVER_ERROR = 500;


//Main URLs
const DOCS = '/docs';
const COMPLETIONS = '/completions';

//Default value for count parameter
const COUNT = 5;

/** Listen on port for incoming requests.  Use docFinder instance
 *  of DocFinder to access document collection methods.
 */
function serve(port, docFinder) {
  const app = express();
  app.locals.port = port;
  app.locals.finder = docFinder;
  setupRoutes(app);
  const server = app.listen(port, async function() {
    console.log(`PID ${process.pid} listening on port ${port}`);
  });
  return server;
}

module.exports = { serve };

function setupRoutes(app) {
  const finder = app.locals.finder;

  app.use(cors());            //for security workaround in future projects
  app.use(bodyParser.json()); //all incoming bodies are JSON
  app.get(`${DOCS}/:name`,doGetContent(app));
  app.get(`${DOCS}?:query`,doSearchContent(app)); 
  app.get(`${COMPLETIONS}?:id`,doGetCompletions(app)); 
  app.post(`${DOCS}`,doAddContent(app));

  app.use(doErrors()); //must be last; setup for server errors   
}

//@TODO: add handler creation functions called by route setup
//routine for each individual web service.  Note that each
//returned handler should be wrapped using errorWrap() to
//ensure that any internal errors are handled reasonably.2
function doGetContent(app) {


  return errorWrap(async function(req, res) {
    try {
      const {name} = req.params;
      const results = await app.locals.finder.docContent(name);

      if (results.length === 0) {
	throw {
	  isDomain: true,
	  errorCode: 'NOT_FOUND',
	  message: `Document ${id} not found`,
	};
      }
      else {
	let links = [];
	let link = {};
	link.rel = 'self';
	link.href = baseUrl(req,req.originalUrl);

	links.push(link);
	let objR = {"content":results,"links":links};
	res.status(OK).json(objR);
	
      }
    }
    catch(err) {
      const mapped = mapError(err);
      res.json(mapped);
    }
  });
}

function doGetCompletions(app) {
  return errorWrap(async function(req, res) {
    try {

      const text = req.query.text;

      if (text === undefined) {
	throw {
	  isDomain: true,
	  errorCode: 'BAD_PARAM',
	  message: `required query parameter \"text\" is missing`,
	};
      }
      else {
      const results = await app.locals.finder.complete(text);
	res.json(results);
      }
    }
    catch(err) {
      const mapped = mapError(err);
      res.json(mapped);
    }
  });
}

function doSearchContent(app) {

  return errorWrap(async function(req, res) {
    try {
	//console.log(req);
      const id = req.query.q;
	//console.log(id);
	if(id == undefined){
	throw {
	  isDomain: true,
	  errorCode: 'BAD_PARAM',
	  message: `required query parameter \"q\" is missing`,
	};
	}
      //const start = req.query.start;
      const count = (req.query.count === undefined) ? 5 : req.query.count ;
      const start = (req.query.start === undefined) ? 0 : req.query.start ;
	//console.log(start);
	//console.log(count);
var regex = RegExp(/(^\d)/);
if(!regex.test(start)){
	throw {
	  isDomain: true,
	  errorCode: 'BAD_PARAM',
	  message: `bad query parameter \"start\"`,
	};
	}
if(!regex.test(count) || parseInt(count) < 0){
	throw {
	  isDomain: true,
	  errorCode: 'BAD_PARAM',
	  message: `bad query parameter \"count\"`,
	};
	}
      let results = await app.locals.finder.find(id);

	let r = [];
	for(let i = 0; i< results.length; i++ ){
		let res = results[i];
		res.href = baseUrl(req,'/docs/'+res.name);
		
	}
        let len = start+count < results.length ? start+count : results.length;
 	for(let i =start; i< len ; i++) {
		r.push(results[i]); 	
	}
	let links = [];
	let link = {};

	link.rel = "self";
	let urlH = baseUrl(req,req.originalUrl);
	//console.log(link);
	link.href = startCount(urlH, start, count);
	links.push(link);
	link = {};
	
	if(/(\&start=)/.test(urlH)) {
		var sarr = urlH.split("&start=");
		//console.log(sarr);
		urlH = sarr[0];
	} else if (/(\&count=)/.test(urlH)) {
		sarr = urlH.split("&count=");
		//console.log(sarr);
		urlH = sarr[0];
	}
	if(start>0) {
		link.rel = "previous"
		if(parseInt(start)-parseInt(count) < 0) {
			//console.log("if");
			var s = 0;
		} else {
			s = parseInt(start)-parseInt(count);
			//console.log("else: ",s);
		}
		link.href = startCount(urlH, s, count)
		links.push(link);
	}
	link = {}
	//console.log("Testing: ", parseInt(start)+parseInt(count));
	if(parseInt(start)+parseInt(count) < results.length) {
		link.rel = "next"
		link.href = startCount(urlH, parseInt(start)+parseInt(count), count)
		links.push(link);
	}
	let objR = {"results":r,"totalCount":results.length,"links":links};
	res.json(objR);
      //}
    }
    catch(err) {
      const mapped = mapError(err);
      res.json(mapped);
    }
  });
}

function startCount(urlH, start, count) {
	if(!(/(\&start=)/.test(urlH))) {
		if(/(\&count=)/.test(urlH)) {
			var sarr = urlH.split("&count=");
			//console.log(sarr);
			urlH = sarr[0];
		}
		urlH = urlH+'&start='+start.toString();
	}
	if(!(/(\&count=)/.test(urlH))) {
		urlH = urlH+'&count='+count.toString();
	}
	return urlH;
}

function doAddContent(app) {

  return async function(req, res) {
    try {
//      console.log(req);	
      const name = req.body.name;
      if(name == undefined){
	throw {
	  isDomain: true,
	  errorCode: 'BAD_PARAM',
	  message: `required body parameter \"name\" is missing`,
	};
      }
      const content = req.body.content;
      if(content == undefined){
	throw {
	  isDomain: true,
	  errorCode: 'BAD_PARAM',
	  message: `required body parameter \"content\" is missing`,
	};
      }	
      //console.log(name+'\n'+content);
      const results = await app.locals.finder.addContent(name,content);
      let link = {};
      link.href = baseUrl(req,'/docs/'+name) 
      res.status(CREATED).json(link);
    }
    catch(err) {
      const mapped = mapError(err);
      res.json(mapped);
    }
  };
}
/** Return error handler which ensures a server error results in nice
 *  JSON sent back to client with details logged on console.
 */ 
function doErrors(app) {
  return async function(err, req, res, next) {
    //res.status(SERVER_ERROR);
    res.json({message: err.message });
    console.error(err);
  };
}

/** Set up error handling for handler by wrapping it in a 
 *  try-catch with chaining to error handler on error.
 */
function errorWrap(handler) {
  return async (req, res, next) => {
    try {
      await handler(req, res, next);
    }
    catch (err) {
      next(err);
    }
  };
}
  
const ERROR_MAP = {
  EXISTS: CONFLICT,
  NOT_FOUND: NOT_FOUND
}

/** Map domain/internal errors into suitable HTTP errors.  Return'd
 *  object will have a "status" property corresponding to HTTP status
 *  code.
 */
function mapError(err) {

  return err.isDomain
    ? { code: err.errorCode,
	message: err.message
      }
    : { code: 'INTERNAL',
	message: err.toString()
      };
} 

/** Return base URL of req for path.
 *  Useful for building links; Example call: baseUrl(req, DOCS)
 */
function baseUrl(req, path='/') {
  const port = req.app.locals.port;
  const url = `${req.protocol}://${req.hostname}:${port}${path}`;
  return url;
}
